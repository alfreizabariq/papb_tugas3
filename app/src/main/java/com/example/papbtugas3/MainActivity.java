package com.example.papbtugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CheckBox cbjanggut, cbkumis, cbrambut, cbalis ;
    ImageView atrKumis, atrJanggut, atrRambut, atrAlis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cbkumis = findViewById(R.id.cbkumis);

        atrKumis = findViewById(R.id.atrKumis);

        cbkumis.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){

        if(view.getId()==atrKumis.getId())
        {
            if(atrKumis.getVisibility()==View.INVISIBLE)
            {
                atrKumis.setVisibility(View.VISIBLE);
            }else{
                atrKumis.setVisibility(View.INVISIBLE);
            }
        }
        if(view.getId()==cbjanggut.getId())
        {
            if(atrJanggut.getVisibility()==View.INVISIBLE)
            {
                atrJanggut.setVisibility(View.VISIBLE);
            }else{
                atrJanggut.setVisibility(View.INVISIBLE);
            }
        }
        if(view.getId()==cbalis.getId())
        {
            if(atrAlis.getVisibility()==View.INVISIBLE)
            {
                atrAlis.setVisibility(View.VISIBLE);
            }else{
                atrAlis.setVisibility(View.INVISIBLE);
            }
        }
        if(view.getId()==cbrambut.getId())
        {
            if(atrRambut.getVisibility()==View.INVISIBLE)
            {
                atrRambut.setVisibility(View.VISIBLE);
            }else{
                atrRambut.setVisibility(View.INVISIBLE);
            }
        }


    }
}